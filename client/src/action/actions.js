import api from '../api/FileSystemExplorerApi';

export const TOGGLE_ENTRY = 'TOGGLE_ENTRY';

export function toggleEntry(entry) {
    return {type: TOGGLE_ENTRY, entry};
}

export const RECEIVE_CHILDREN = 'RECEIVE_CHILDREN';

export function receiveChildren(entry, children) {
    return {type: RECEIVE_CHILDREN, entry, children};
}

export const RECEIVE_ROOT = 'RECEIVE_ROOT';

export function receiveRoot(root) {
    return {type: RECEIVE_ROOT, root};
}

export function fetchChildren(entry) {
    return async (dispatch) => {
        const entryPath = entry.basePath === undefined ? '' : `${entry.basePath}/${entry.name}`;
        try {
            const response = await api.listPath(entryPath);
            return dispatch(
                receiveChildren(
                    entry,
                    response.data.sort((left, right) => {
                        if (left.name < right.name) return -1;
                        if (left.name > right.name) return 1;
                        return 0;
                    })
                )
            );
        } catch (e) {
            alert(e.response.data.message);
            return dispatch(toggleEntry(entry));
        }
    };
}

export function fetchRoot() {
    return async (dispatch) => {
        const response = await api.fetchRoot();
        return dispatch(receiveRoot(response.data));
    };
}