import axios from 'axios';

async function fetchRoot() {
    return axios.get('/root');
}

async function listPath(path) {
    return axios.get(`/list/${path}`);
}

const api = {fetchRoot, listPath};

export default api;