import '../action/actions';
import './file-system-entry/FileSystemEntryList';
import FileSystemEntryList from './file-system-entry/FileSystemEntryList';
import * as React from 'react';
import {fetchChildren, fetchRoot, toggleEntry} from '../action/actions';
import {connect} from 'react-redux';

class FileSystemExplorer extends React.Component {
    componentDidMount() {
        this.fetchRootIfNeeded();
    }

    render() {
        if (this.props.root === undefined) {
            return null;
        }
        return (
            <FileSystemEntryList items={[this.props.root]}
                                 toggleEntry={this.props.toggleEntry}
                                 fetchChildren={this.props.fetchChildren}
            />
        );
    }

    fetchRootIfNeeded() {
        if (this.props.root === undefined) {
            this.props.fetchRoot();
        }
    }
}

const mapStateToProps = state => ({root: state.root});
const mapDispatchToProps = {toggleEntry, fetchRoot, fetchChildren};

export default connect(mapStateToProps, mapDispatchToProps)(FileSystemExplorer);