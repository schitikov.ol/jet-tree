import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import React from 'react';
import FileSystemEntryTypes from './FileSystemEntryTypes';
import {
    faBox,
    faBoxOpen,
    faFile,
    faFileAlt,
    faFileAudio,
    faFileImage,
    faFileVideo,
    faFolder,
    faFolderOpen,
    faScroll
} from '@fortawesome/free-solid-svg-icons';

export default function FileSystemEntryIcon(props) {
    return (
        <FontAwesomeIcon icon={getFileSystemEntryFaIcon(props.entry)}
                         className='file-system-entry__name-box__icon'
                         fixedWidth

        />
    );
}

function getFileSystemEntryFaIcon(entry) {
    switch (entry.type) {
        case FileSystemEntryTypes.DIRECTORY:
            return entry.expanded ? faFolderOpen : faFolder;
        case FileSystemEntryTypes.ARCHIVE:
            return entry.expanded ? faBoxOpen : faBox;
        case FileSystemEntryTypes.IMAGE:
            return faFileImage;
        case FileSystemEntryTypes.VIDEO:
            return faFileVideo;
        case FileSystemEntryTypes.TEXT:
            return faFileAlt;
        case FileSystemEntryTypes.AUDIO:
            return faFileAudio;
        case FileSystemEntryTypes.EXEC:
            return faScroll;
        default:
            return faFile;
    }
}