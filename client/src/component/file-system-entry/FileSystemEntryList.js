import React from 'react';
import FileSystemEntry from './FileSystemEntry';
import './FileSystemEntryList.css';

export default function FileSystemEntryList(props) {
    const listItems = props.items.map(item => {
        return (
            <li key={item.name}>
                <FileSystemEntry entry={item} toggleEntry={props.toggleEntry} fetchChildren={props.fetchChildren}/>
            </li>
        );
    });
    return <ul className='file-system-entry-list'>{listItems}</ul>;
}