import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCircleNotch} from '@fortawesome/free-solid-svg-icons';
import React from 'react';

export default function FileSystemEntryLoaderSpinner() {
    return <FontAwesomeIcon className='file-system-entry__name-box__icon' icon={faCircleNotch} size='xs' spin/>;
}