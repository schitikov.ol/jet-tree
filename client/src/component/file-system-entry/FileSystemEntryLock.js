import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faLock} from '@fortawesome/free-solid-svg-icons';
import React from 'react';

export default function FileSystemEntryLock() {
    return <FontAwesomeIcon className='file-system-entry__name-box__icon' icon={faLock} size='xs'/>;
}