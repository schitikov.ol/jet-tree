import React from 'react';
import './FileSystemEntry.css';
import './FileSystemEntryList';
import FileSystemEntryList from './FileSystemEntryList';
import FileSystemEntryTypes from './FileSystemEntryTypes';
import FileSystemEntryIcon from './FileSystemEntryIcon';
import FileSystemEntryLoaderSpinner from './FileSystemEntryLoaderSpinner';
import FileSystemEntryLock from './FileSystemEntryLock';

export default class FileSystemEntry extends React.Component {

    componentDidMount() {
        this.fetchChildrenIfNeeded();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.fetchChildrenIfNeeded();
    }

    render() {
        const entry = this.props.entry;

        return (
            <div className='file-system-entry'>
                <div className='file-system-entry__name-box' onClick={e => {
                    e.stopPropagation();
                    if (entry.expandable) {
                        this.props.toggleEntry(entry);
                    }
                }}>
                    <FileSystemEntryIcon entry={entry}/>
                    <span className='file-system-entry__name-box__name'>{entry.name}</span>
                    {renderLockIfNeeded(entry)}
                    {renderLoadingSpinnerIfNeeded(entry)}
                </div>
                {this.renderFileSystemEntryListIfNeeded(entry)}
            </div>
        );
    }

    renderFileSystemEntryListIfNeeded(entry) {
        if (entry.expandable && entry.expanded && entry.children !== undefined) {
            return (
                <FileSystemEntryList items={this.props.entry.children}
                                     toggleEntry={this.props.toggleEntry}
                                     fetchChildren={this.props.fetchChildren}
                />
            );
        }

        return null;
    }

    fetchChildrenIfNeeded() {
        const entry = this.props.entry;

        if (entry.expanded && entry.children === undefined) {
            this.props.fetchChildren(entry);
        }
    }
}

function renderLoadingSpinnerIfNeeded(entry) {
    if (entry.expanded && entry.children === undefined) {
        return <FileSystemEntryLoaderSpinner/>;
    }

    return null;
}

function renderLockIfNeeded(entry) {
    if ((entry.type === FileSystemEntryTypes.DIRECTORY || entry.type === FileSystemEntryTypes.ARCHIVE) && !entry.expandable) {
        return <FileSystemEntryLock/>;
    }

    return null;
}
