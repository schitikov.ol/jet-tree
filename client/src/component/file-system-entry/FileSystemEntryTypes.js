const ARCHIVE = 'ARCHIVE';
const FILE = 'FILE';
const DIRECTORY = 'DIRECTORY';
const IMAGE = 'IMAGE';
const VIDEO = 'VIDEO';
const TEXT = 'TEXT';
const AUDIO = 'AUDIO';
const EXEC = 'EXEC';

const FileSystemEntryTypes = {ARCHIVE, FILE, DIRECTORY, IMAGE, VIDEO, TEXT, AUDIO, EXEC};

export default FileSystemEntryTypes;