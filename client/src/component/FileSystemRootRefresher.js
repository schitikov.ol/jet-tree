import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import * as React from 'react';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {fetchRoot} from '../action/actions';
import {connect} from 'react-redux';
import './FileSystemRootRefresher.css';

function FileSystemRootRefresher(props) {
    return (
        <span className='file-system-root-refresher' onClick={props.fetchRoot}>
            <FontAwesomeIcon className='file-system-root-refresher__icon' icon={faSyncAlt}/>
            <span>Click to refresh root</span>
        </span>
    );
}

const mapDispatchToProps = {fetchRoot};

export default connect(null, mapDispatchToProps)(FileSystemRootRefresher);