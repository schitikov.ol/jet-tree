import '../action/actions';
import {RECEIVE_CHILDREN, RECEIVE_ROOT, TOGGLE_ENTRY} from '../action/actions';

export default function fileSystemExplorerReducer(state, action) {
    switch (action.type) {
        case TOGGLE_ENTRY:
            return toggleEntryReducer(state, action);
        case RECEIVE_CHILDREN:
            return receiveChildrenReducer(state, action);
        case RECEIVE_ROOT: {
            return receiveRootReducer(state, action);
        }
        default:
            return state;
    }
}

function toggleEntryReducer(state, action) {
    switch (action.type) {
        case TOGGLE_ENTRY: {
            const entry = action.entry;
            return {...state, root: updateNode(state.root, 0, entry, {expanded: !entry.expanded, children: undefined})};
        }
        default:
            return state;
    }
}

function receiveChildrenReducer(state, action) {
    switch (action.type) {
        case RECEIVE_CHILDREN: {
            const entry = action.entry;
            return {...state, root: updateNode(state.root, 0, entry, {children: action.children})};
        }
        default:
            return state;
    }
}

function receiveRootReducer(state, action) {
    switch (action.type) {
        case RECEIVE_ROOT: {
            return {...state, root: action.root};
        }
        default:
            return state;
    }
}

function updateNode(currentRoot, currentPathSegmentIndex, targetNode, fieldsToUpdate) {
    if (entriesMatches(currentRoot, targetNode)) {
        return {...currentRoot, ...fieldsToUpdate};
    }

    const targetNodeFullPathSegments = targetNode.basePath === '' ?
        [targetNode.name] : `${targetNode.basePath}/${targetNode.name}`.split('/');
    const children = currentRoot.children;

    const nextPathSegmentIndex = currentPathSegmentIndex + 1;
    const nextRootIndex = children.findIndex((child) => child.name === targetNodeFullPathSegments[currentPathSegmentIndex]);

    const updatedChild = updateNode(children[nextRootIndex], nextPathSegmentIndex, targetNode, fieldsToUpdate);

    return {...currentRoot, children: newArrayWithItemReplaced(children, updatedChild, nextRootIndex)};
}

function entriesMatches(one, another) {
    return one.basePath + one.name === another.basePath + another.name;
}

function newArrayWithItemReplaced(array, item, itemIndex) {
    const newArray = [...array];
    newArray.splice(itemIndex, 1, item);
    return newArray;
}