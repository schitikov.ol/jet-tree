import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import './reducer/reducers';
import fileSystemExplorerReducer from './reducer/reducers';
import {applyMiddleware, createStore} from 'redux';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {logger} from 'redux-logger';
import {PersistGate} from 'redux-persist/es/integration/react';
import storage from 'redux-persist/lib/storage';
import {persistReducer, persistStore} from 'redux-persist';
import './component/FileSystemExplorer';
import './component/FileSystemRootRefresher';
import './index.css';
import FileSystemRootRefresher from './component/FileSystemRootRefresher';
import FileSystemExplorer from './component/FileSystemExplorer';

const persistConfig = {
    key: 'root',
    storage
};

const persistedReducer = persistReducer(persistConfig, fileSystemExplorerReducer);

const store = createStore(persistedReducer, applyMiddleware(thunk, logger));

render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistStore(store)}>
            <FileSystemRootRefresher/>
            <FileSystemExplorer/>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);
