# Jet Tree Project

## Tools:
+ **Backend:** Kotlin, Spring Boot 2 (Web)
+ **Frontend:** React, Redux, Font Awesome
+ **Building:** Gradle

## Features:
+ The app represents a tree-like folder structure of some filesystem
+ Watch your filesystem relative to the specified root in your browser
+ Support zip expanding
+ Lazy folders loading
+ Save representational state after page reloading/closing

## Building and start-up:

In the project root do the following in a console:  
1. Go to the server dir: `cd server`
2. Build the server.

   On Windows: `./gradlew.bat bootJar`.  
   On Linux/Mac: `./gradlew bootJar`
3. Run the server: `java -jar build/libs/server-0.0.1-SNAPSHOT.jar --rootPath=/some/folder`.

   **Note**: rootPath must be absolute. If you don't specify rootPath then user home directory will be used by default.
4. Go to the client dir: `cd ../client`
5. Install client dependencies: `npm install`
6. Run the client: `npm start`
   
App is deployed at `http://localhost:3000`

**Note**: If you restarted the server and changed the rootPath, click on "Click to refresh root" on the client.

## Screenshots:
![](screenshots/tree.gif)
