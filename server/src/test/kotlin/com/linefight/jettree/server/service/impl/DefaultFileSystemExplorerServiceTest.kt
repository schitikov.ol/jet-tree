package com.linefight.jettree.server.service.impl

import com.linefight.jettree.server.extension.PATH_EMPTY
import com.linefight.jettree.server.extension.invariantSeparatorsPath
import com.linefight.jettree.server.model.FileSystemEntry
import com.linefight.jettree.server.model.FileSystemEntryType
import com.linefight.jettree.server.model.FileSystemRootEntry
import com.linefight.jettree.server.repository.FileSystemRepository
import com.linefight.jettree.server.service.FileSystemEntryTypeService
import com.linefight.jettree.server.validation.exception.PathNotExistsValidationException
import com.linefight.jettree.server.validation.exception.PathNotExpandableValidationException
import com.linefight.jettree.server.validation.exception.PathNotInRootValidationException
import com.linefight.jettree.server.validation.impl.DefaultFileSystemPathValidator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

@SpringBootTest
class DefaultFileSystemExplorerServiceTest {

    private lateinit var service: DefaultFileSystemExplorerService

    private lateinit var rootPath: Path

    @Autowired
    private lateinit var entryTypeService: FileSystemEntryTypeService

    @Autowired
    private lateinit var fileSystemRepository: FileSystemRepository

    @BeforeEach
    fun setUp() {
        rootPath = Paths.get(javaClass.classLoader.getResource("root")!!.toURI())
        service = createDefaultFileSystemExplorerService(rootPath)
    }

    @Test
    fun `test getting root entry descriptor`() {
        val rootEntry = service.getRoot()

        assertEquals(rootPath.fileName.toString(), rootEntry.name)
    }

    @Test
    fun `test listing root path`() {
        val basePath = PATH_EMPTY

        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "nestedDir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "archive.zip",
                        type = FileSystemEntryType.ARCHIVE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "image_bmp.bmp",
                        type = FileSystemEntryType.IMAGE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "image_jpg.jpg",
                        type = FileSystemEntryType.IMAGE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "root.file",
                        type = FileSystemEntryType.FILE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "text.txt",
                        type = FileSystemEntryType.TEXT,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "video_mp4.mp4",
                        type = FileSystemEntryType.VIDEO,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "wow_so_compressed.zip",
                        type = FileSystemEntryType.ARCHIVE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "dotted.dir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                )
        )

        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing nested path`() {
        val basePath = "nestedDir".toPath()
        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "root.nestedDir.file",
                        type = FileSystemEntryType.FILE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "nestedDir_1",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "audio.zip.mp3",
                        type = FileSystemEntryType.AUDIO,
                        isReadable = true
                )
        )

        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing double nested path`() {
        val basePath = "nestedDir/nestedDir_1".toPath()
        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "image_bmp.bmp",
                        type = FileSystemEntryType.IMAGE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "image_jpg.jpg",
                        type = FileSystemEntryType.IMAGE,
                        isReadable = true
                )
        )

        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing zip archive first layer`() {
        val basePath = "archive.zip".toPath()
        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "nestedDir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "dotted.dir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                )
        )

        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing zip archive second layer`() {
        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = "archive.zip/nestedDir".toPath(),
                        name = "root.nestedDir.file",
                        type = FileSystemEntryType.FILE,
                        isReadable = true
                )
        )
        val actualEntries = service.listPath("archive.zip/nestedDir".toPath()).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing nested zip first layer`() {
        val basePath = "wow_so_compressed.zip"

        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath.toPath(),
                        name = "nestedDir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath.toPath(),
                        name = "archive.zip",
                        type = FileSystemEntryType.ARCHIVE,
                        isReadable = true
                )
        )
        val actualEntries = service.listPath(basePath.toPath()).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing nested zip second layer`() {
        val basePath = "wow_so_compressed.zip/nestedDir".toPath()

        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "nestedDir_1",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "media.zip",
                        type = FileSystemEntryType.ARCHIVE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "root.nestedDir.file",
                        type = FileSystemEntryType.FILE,
                        isReadable = true
                )
        )
        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing nested zip third layer`() {
        val basePath = "wow_so_compressed.zip/nestedDir/media.zip".toPath()

        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "image_bmp.bmp",
                        type = FileSystemEntryType.IMAGE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "image_jpg.jpg",
                        type = FileSystemEntryType.IMAGE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "root.file",
                        type = FileSystemEntryType.FILE,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "text.txt",
                        type = FileSystemEntryType.TEXT,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "video_mp4.mp4",
                        type = FileSystemEntryType.VIDEO,
                        isReadable = true
                )
        )
        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    @Test
    fun `test listing path started with slash`() {
        assertThrows<PathNotInRootValidationException> {
            service.listPath("/".toPath())
        }

        assertThrows<PathNotInRootValidationException> {
            service.listPath("/".toPath())
        }
        assertThrows<PathNotInRootValidationException> {
            service.listPath("/dir".toPath())
        }
    }

    @Test
    fun `test listing parent of root parent directory`() {
        assertThrows<PathNotInRootValidationException> {
            service.listPath("..".toPath())
        }
        assertThrows<PathNotInRootValidationException> {
            service.listPath("../..".toPath())
        }
        assertThrows<PathNotInRootValidationException> {
            service.listPath("dir/../..".toPath())
        }
    }

    @Test
    fun `test listing not expandable path`() {
        assertThrows<PathNotExpandableValidationException> {
            service.listPath("root.file".toPath())
        }
    }

    @Test
    fun `test listing not existing path`() {
        assertThrows<PathNotExistsValidationException> {
            service.listPath("root/${UUID.randomUUID()}".toPath())
        }
    }

    @Test
    fun `test getting root entry when root path is file system root`() {
        val rootPath = Paths.get("/").toRealPath()

        val service = createDefaultFileSystemExplorerService(rootPath)

        val expectedRoot = FileSystemRootEntry(
                name = rootPath.invariantSeparatorsPath,
                type = FileSystemEntryType.DIRECTORY,
                isReadable = true
        )
        val actualRoot = service.getRoot()

        assertEquals(expectedRoot, actualRoot)
    }

    @Test
    fun `test listing path when root path is file system root`() {
        val rootPath = Paths.get("/").toRealPath()

        val service = createDefaultFileSystemExplorerService(rootPath)

        val entries = service.listPath(PATH_EMPTY)

        assertTrue(entries.isNotEmpty())
    }

    @Test
    fun `test getting root entry when root path is archive`() {
        val rootPath = this.rootPath.resolve("archive.zip")

        val service = createDefaultFileSystemExplorerService(rootPath)

        val expectedRoot = FileSystemRootEntry(
                name = rootPath.fileName.toString(),
                type = FileSystemEntryType.ARCHIVE,
                isReadable = true
        )
        val actualRoot = service.getRoot()

        assertEquals(expectedRoot, actualRoot)
    }

    @Test
    fun `test listing path when root path is archive`() {
        val rootPath = this.rootPath.resolve("archive.zip")

        val service = createDefaultFileSystemExplorerService(rootPath)

        val basePath = "".toPath()

        val expectedEntries = setOf(
                FileSystemEntry(
                        basePath = basePath,
                        name = "nestedDir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                ),
                FileSystemEntry(
                        basePath = basePath,
                        name = "dotted.dir",
                        type = FileSystemEntryType.DIRECTORY,
                        isReadable = true
                )
        )
        val actualEntries = service.listPath(basePath).toSet()

        assertEquals(expectedEntries, actualEntries)
    }

    private fun createDefaultFileSystemExplorerService(rootPath: Path): DefaultFileSystemExplorerService =
            DefaultFileSystemExplorerService(
                    rootPath,
                    DefaultFileSystemPathValidator(
                            rootPath
                    ),
                    entryTypeService,
                    fileSystemRepository
            )

    private fun String.toPath(): Path = Paths.get(this)
}