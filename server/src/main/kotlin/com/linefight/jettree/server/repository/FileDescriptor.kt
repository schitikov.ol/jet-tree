package com.linefight.jettree.server.repository

import java.nio.file.Path

data class FileDescriptor(
        val basePath: Path,
        val name: String,
        val isDirectory: Boolean,
        val isReadable: Boolean
)