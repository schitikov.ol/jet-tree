package com.linefight.jettree.server.validation

import java.nio.file.Path

/**
 * Interface for path validator of some filesystem
 */
interface FileSystemPathValidator {
    /**
     * @param path [Path] to validate
     * @throws [com.linefight.jettree.server.validation.exception.ValidationException] if [path] is not valid
     */
    fun validate(path: Path)

    /**
     * @param path [Path] to validate
     * @throws [com.linefight.jettree.server.validation.exception.ValidationException] if [path] is not valid
     */
    fun validate(path: String)
}