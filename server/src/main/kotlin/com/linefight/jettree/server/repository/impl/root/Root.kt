package com.linefight.jettree.server.repository.impl.root

import com.linefight.jettree.server.repository.FileDescriptor
import java.io.InputStream
import java.nio.file.Path

/**
 * Interface for an abstract source of file hierarchy
 * @property path absolute path to this root
 */
interface Root {
    val path: Path

    /**
     * @param relativePath path relative to this root
     * @return [List] of [FileDescriptor] contained in [relativePath]
     * @throws [com.linefight.jettree.server.repository.exception.PathNotExistsException] if [relativePath] doesn't exist
     * @throws [com.linefight.jettree.server.repository.exception.PathNotListableException] if [relativePath] is not listable
     */
    fun listPath(relativePath: Path): List<FileDescriptor>

    /**
     * @param relativePath path relative to this root
     * @return [FileDescriptor] of [relativePath] or null if it doesn't exist
     * @throws [com.linefight.jettree.server.repository.exception.PathNotExistsException] if [relativePath] doesn't exist
     */
    fun getFileDescriptor(relativePath: Path): FileDescriptor

    /**
     * @param relativePath path relative to this root
     * @return [InputStream] for [relativePath] or null if it doesn't exist
     * @throws [com.linefight.jettree.server.repository.exception.PathNotExistsException] if [relativePath] doesn't exist
     */
    fun inputStreamFor(relativePath: Path): InputStream
}