package com.linefight.jettree.server.repository.impl

import com.linefight.jettree.server.extension.PATH_EMPTY
import com.linefight.jettree.server.extension.extension
import com.linefight.jettree.server.extension.invariantSeparatorsPath
import com.linefight.jettree.server.repository.FileDescriptor
import com.linefight.jettree.server.repository.FileSystemRepository
import com.linefight.jettree.server.repository.exception.PathNotExistsException
import com.linefight.jettree.server.repository.impl.root.DirRoot
import com.linefight.jettree.server.repository.impl.root.Root
import com.linefight.jettree.server.repository.impl.root.RootType
import org.springframework.stereotype.Component
import java.nio.file.Path

/**
 * Represents read-only file system repository of the local machine
 */
@Component
class DefaultFileSystemRepository : FileSystemRepository {
    private val Path.isSupportedFile get() = extension in RootType.supportedExtensions

    override fun getFileDescriptor(path: Path): FileDescriptor {
        require(path.isAbsolute) { "Path must be absolute. Path: $path" }
        val pathParent = path.parent ?: return getRootFileDescriptor(path)

        return listPath(pathParent).find { it.name == path.fileName.toString() } ?: throw PathNotExistsException(path)
    }

    override fun listPath(path: Path): List<FileDescriptor> {
        require(path.isAbsolute) { "Path must be absolute. Path: $path" }
        val root = DirRoot(path.root)
        return doListPath(root, root.path.relativize(path))
    }

    private fun getRootFileDescriptor(path: Path): FileDescriptor = FileDescriptor(
            basePath = PATH_EMPTY,
            name = path.root.invariantSeparatorsPath,
            isDirectory = true,
            isReadable = true
    )

    private fun doListPath(root: Root, relativePath: Path): List<FileDescriptor> {
        val relativePathToFirstFile = pathToFirstRootFile(root, relativePath) ?: return root.listPath(relativePath)

        return doListPath(
                RootType.fromPath(relativePathToFirstFile).createRoot(root, relativePathToFirstFile),
                relativePathToFirstFile.relativize(relativePath)
        )
    }

    private fun pathToFirstRootFile(root: Root, relativePath: Path): Path? {
        if (relativePath == PATH_EMPTY) return null

        var pathSegmentAccumulator = PATH_EMPTY

        for (pathSegment in relativePath) {
            pathSegmentAccumulator = pathSegmentAccumulator.resolve(pathSegment)
            if (pathSegmentAccumulator.isSupportedFile && !root.getFileDescriptor(pathSegmentAccumulator).isDirectory) {
                return pathSegmentAccumulator
            }
        }

        return null
    }
}

