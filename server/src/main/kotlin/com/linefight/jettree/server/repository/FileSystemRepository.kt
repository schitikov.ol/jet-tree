package com.linefight.jettree.server.repository

import java.nio.file.Path

/**
 * Interface for read-only file system repository
 */
interface FileSystemRepository {
    /**
     * @param path absolute path
     * @return [FileDescriptor] for [path]
     * @throws [com.linefight.jettree.server.repository.exception.PathNotExistsException] if [path] doesn't exist
     */
    fun getFileDescriptor(path: Path): FileDescriptor

    /**
     * @param path absolute path
     * @return [List] of [FileDescriptor] contained in [path]
     * @throws [com.linefight.jettree.server.repository.exception.PathNotExistsException] if [path] doesn't exist
     * @throws [com.linefight.jettree.server.repository.exception.PathNotListableException] if [path] is not listable
     */
    fun listPath(path: Path): List<FileDescriptor>
}