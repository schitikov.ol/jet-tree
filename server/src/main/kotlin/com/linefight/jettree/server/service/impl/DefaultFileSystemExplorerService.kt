package com.linefight.jettree.server.service.impl

import com.linefight.jettree.server.model.FileSystemEntry
import com.linefight.jettree.server.model.FileSystemRootEntry
import com.linefight.jettree.server.repository.FileDescriptor
import com.linefight.jettree.server.repository.FileSystemRepository
import com.linefight.jettree.server.repository.exception.PathNotExistsException
import com.linefight.jettree.server.repository.exception.PathNotListableException
import com.linefight.jettree.server.service.FileSystemEntryTypeService
import com.linefight.jettree.server.service.FileSystemExplorerService
import com.linefight.jettree.server.validation.FileSystemPathValidator
import com.linefight.jettree.server.validation.exception.PathNotExistsValidationException
import com.linefight.jettree.server.validation.exception.PathNotExpandableValidationException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.nio.file.Path

@Service
/**
 * Represents file hierarchy explorer relative to [rootPath]
 * @property rootPath Absolute [Path] to the file hierarchy root
 */
class DefaultFileSystemExplorerService(
        private val rootPath: Path,
        private val pathValidator: FileSystemPathValidator,
        private val entryTypeService: FileSystemEntryTypeService,
        private val fileSystemRepository: FileSystemRepository
) : FileSystemExplorerService {
    private val log: Logger = LoggerFactory.getLogger(DefaultFileSystemExplorerService::class.java)

    override fun getRoot(): FileSystemRootEntry {
        val rootPathParent = rootPath.parent
                ?: return fileSystemRepository.getFileDescriptor(rootPath.root).toFileSystemRootEntry()

        return fileSystemRepository.listPath(rootPathParent)
                .find { it.name == rootPath.fileName.toString() }!!.toFileSystemRootEntry()
    }

    override fun listPath(path: Path): List<FileSystemEntry> {
        log.debug("Validating path: {}", path)

        pathValidator.validate(path)

        val fullPath = rootPath.resolve(path).normalize()

        log.debug("Listing files in path: {}", fullPath)

        val files = tryListPath(fullPath)

        log.debug("Result: {}", files)

        return files
    }

    private fun tryListPath(fullPath: Path): List<FileSystemEntry> = try {
        fileSystemRepository.listPath(fullPath).map { it.toFileSystemEntry() }
    } catch (ex: PathNotExistsException) {
        throw PathNotExistsValidationException(rootPath.relativize(ex.path))
    } catch (ex: PathNotListableException) {
        throw PathNotExpandableValidationException(rootPath.relativize(ex.path))
    }

    private fun FileDescriptor.toFileSystemEntry() = FileSystemEntry(
            basePath = rootPath.relativize(basePath),
            name = name,
            isReadable = isReadable,
            type = entryTypeService.resolveFileSystemEntryType(this)
    )

    private fun FileDescriptor.toFileSystemRootEntry() = FileSystemRootEntry(
            name = name,
            isReadable = isReadable,
            type = entryTypeService.resolveFileSystemEntryType(this)
    )
}