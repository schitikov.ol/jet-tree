package com.linefight.jettree.server.exception

open class ServiceException(message: String? = null, cause: Throwable? = null) :
        RuntimeException(message, cause)