package com.linefight.jettree.server.controller

import com.linefight.jettree.server.model.FileSystemEntry
import com.linefight.jettree.server.model.FileSystemRootEntry
import javax.servlet.http.HttpServletRequest

interface FileSystemExplorerController {
    fun listPath(request: HttpServletRequest): List<FileSystemEntry>
    fun getRoot(): FileSystemRootEntry
}