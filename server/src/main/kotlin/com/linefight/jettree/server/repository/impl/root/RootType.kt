package com.linefight.jettree.server.repository.impl.root

import com.linefight.jettree.server.extension.extension
import com.linefight.jettree.server.repository.exception.PathNotListableException
import java.nio.file.Files
import java.nio.file.Path

enum class RootType(val extensions: Set<String>) {
    ZIP(setOf("zip", "war", "jar")) {
        override fun createRoot(parentRoot: Root, relativePathFromParentRoot: Path): Root =
                ZipRoot(parentRoot, relativePathFromParentRoot)
    },
    DIR(emptySet()) {
        override fun createRoot(parentRoot: Root, relativePathFromParentRoot: Path): Root =
                DirRoot(parentRoot.path.resolve(relativePathFromParentRoot))
    };

    companion object {
        val supportedExtensions: Set<String> = values().flatMap { it.extensions }.toSet()

        fun fromPath(path: Path): RootType {
            if (Files.isDirectory(path)) return DIR

            return values().find { path.extension in it.extensions }
                    ?: throw PathNotListableException(path)
        }
    }

    abstract fun createRoot(parentRoot: Root, relativePathFromParentRoot: Path): Root
}