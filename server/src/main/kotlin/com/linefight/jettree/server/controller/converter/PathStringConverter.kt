package com.linefight.jettree.server.controller.converter

import com.fasterxml.jackson.databind.util.StdConverter
import com.linefight.jettree.server.extension.invariantSeparatorsPath
import java.nio.file.Path

object PathStringConverter : StdConverter<Path, String>() {
    override fun convert(value: Path): String = value.invariantSeparatorsPath
}