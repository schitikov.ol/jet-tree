package com.linefight.jettree.server.repository.impl.root

import java.io.InputStream
import java.nio.file.Path

/**
 * Represent an abstract file source of file hierarchy (i.e. archives)
 * @property parentRoot [Root] which contains this [FileRoot]
 */
abstract class FileRoot(val parentRoot: Root, private val relativePathFromParentRoot: Path) : Root {
    override val path: Path = parentRoot.path.resolve(relativePathFromParentRoot)

    /**
     * @return [InputStream] from [parentRoot] for this [FileRoot]
     */
    protected fun inputStreamForThis(): InputStream = parentRoot.inputStreamFor(relativePathFromParentRoot)

    override fun toString(): String {
        return "FileRoot(parentRoot=$parentRoot)"
    }
}