package com.linefight.jettree.server.model

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.linefight.jettree.server.controller.converter.PathStringConverter
import java.nio.file.Path

data class FileSystemEntry(
        @JsonSerialize(converter = PathStringConverter::class)
        val basePath: Path,
        val name: String,
        val type: FileSystemEntryType,
        private val isReadable: Boolean
) {
    val isExpandable = isReadable && type.isExpandable
}