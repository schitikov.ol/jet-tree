package com.linefight.jettree.server.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.nio.file.Path
import java.nio.file.Paths

@Configuration
class SpringConfiguration {
    @Bean
    fun getRootPath(@Value(value = "\${rootPath:}") rootPath: String): Path {
        if (rootPath.isBlank()) {
            return Paths.get(System.getProperty("user.home")).toAbsolutePath()
        }

        val root = Paths.get(rootPath)

        check(root.isAbsolute) { "Root path must be absolute. Root path: $rootPath" }

        return root
    }
}