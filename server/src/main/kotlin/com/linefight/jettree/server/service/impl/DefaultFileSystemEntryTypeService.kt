package com.linefight.jettree.server.service.impl

import com.linefight.jettree.server.configuration.FileSystemEntryTypeProperties
import com.linefight.jettree.server.model.FileSystemEntryType
import com.linefight.jettree.server.repository.FileDescriptor
import com.linefight.jettree.server.service.FileSystemEntryTypeService
import org.springframework.stereotype.Service

@Service
class DefaultFileSystemEntryTypeService(private val entryTypeProperties: FileSystemEntryTypeProperties) :
        FileSystemEntryTypeService {

    override fun resolveFileSystemEntryType(fileDescriptor: FileDescriptor): FileSystemEntryType {
        if (fileDescriptor.isDirectory) return FileSystemEntryType.DIRECTORY

        return entryTypeProperties.getFileSystemEntryType(
                fileDescriptor.name.substringAfterLast('.', missingDelimiterValue = "")
        ) ?: FileSystemEntryType.FILE
    }
}