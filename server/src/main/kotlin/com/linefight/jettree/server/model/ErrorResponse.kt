package com.linefight.jettree.server.model

data class ErrorResponse(val errorCode: ErrorCode, val message: String? = null)

enum class ErrorCode {
    PATH_INVALID,
    PATH_NOT_EXISTS,
    PATH_NOT_EXPANDABLE,
    PATH_NOT_IN_ROOT,
    INTERNAL_SERVER_ERROR
}