package com.linefight.jettree.server.service

import com.linefight.jettree.server.model.FileSystemEntryType
import com.linefight.jettree.server.repository.FileDescriptor

interface FileSystemEntryTypeService {
    fun resolveFileSystemEntryType(fileDescriptor: FileDescriptor): FileSystemEntryType
}