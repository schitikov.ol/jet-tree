package com.linefight.jettree.server.model

enum class FileSystemEntryType(val isExpandable: Boolean) {
    DIRECTORY(true),
    ARCHIVE(true),
    IMAGE(false),
    VIDEO(false),
    AUDIO(false),
    TEXT(false),
    FILE(false),
    EXEC(false)
}
