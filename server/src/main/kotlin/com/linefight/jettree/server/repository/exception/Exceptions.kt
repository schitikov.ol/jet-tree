package com.linefight.jettree.server.repository.exception

import java.nio.file.Path

class PathNotExistsException(val path: Path) : RuntimeException("Path not exists. Path: $path")

class PathNotListableException(val path: Path) : RuntimeException("Path is not listable. Path: $path")