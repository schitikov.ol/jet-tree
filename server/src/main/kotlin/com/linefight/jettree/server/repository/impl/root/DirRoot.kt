package com.linefight.jettree.server.repository.impl.root

import com.linefight.jettree.server.extension.getOrEmpty
import com.linefight.jettree.server.repository.FileDescriptor
import com.linefight.jettree.server.repository.exception.PathNotExistsException
import com.linefight.jettree.server.repository.exception.PathNotListableException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path
import kotlin.streams.toList

/**
 * Represents directory root of file hierarchy
 */
class DirRoot(override val path: Path) : Root {
    private val log: Logger = LoggerFactory.getLogger(DirRoot::class.java)

    override fun listPath(relativePath: Path): List<FileDescriptor> {
        val basePath = path.resolve(relativePath)
        log.debug("Listing path: {}. Root path: {}.", relativePath, path)

        validatePathExists(basePath)
        validatePathListable(basePath)

        val fileDescriptors = Files.list(basePath).map { it.toFileDescriptor() }.toList()

        log.debug("File descriptors: {}", fileDescriptors)

        return fileDescriptors
    }

    override fun getFileDescriptor(relativePath: Path): FileDescriptor {
        val fullPath = path.resolve(relativePath)
        log.debug("Getting file descriptor for path: {}. Root path: {}.", relativePath, path)

        validatePathExists(fullPath)

        return fullPath.toFileDescriptor()
    }

    override fun inputStreamFor(relativePath: Path): InputStream {
        log.debug("Getting input stream for path: {}. Root path: {}", relativePath, path)

        val fullPath = path.resolve(relativePath)

        validatePathExists(fullPath)

        return fullPath.toFile().inputStream()
    }

    override fun toString(): String {
        return "DirRoot(path=$path)"
    }

    private fun Path.toFileDescriptor() = FileDescriptor(
            basePath = parent.getOrEmpty(),
            name = fileName.toString(),
            isDirectory = Files.isDirectory(this),
            isReadable = Files.isReadable(this)
    )

    private fun validatePathExists(path: Path) {
        if (Files.notExists(path)) {
            throw PathNotExistsException(path)
        }
    }

    private fun validatePathListable(path: Path) {
        if (!Files.isDirectory(path)) {
            throw PathNotListableException(path)
        }
    }
}