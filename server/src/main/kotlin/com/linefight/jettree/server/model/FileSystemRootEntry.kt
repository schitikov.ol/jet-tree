package com.linefight.jettree.server.model

data class FileSystemRootEntry(
        val name: String,
        val type: FileSystemEntryType,
        private val isReadable: Boolean
) {
    val isExpandable = isReadable && type.isExpandable
}