package com.linefight.jettree.server.controller.impl

import com.linefight.jettree.server.controller.FileSystemExplorerController
import com.linefight.jettree.server.model.FileSystemEntry
import com.linefight.jettree.server.model.FileSystemRootEntry
import com.linefight.jettree.server.service.FileSystemExplorerService
import com.linefight.jettree.server.validation.FileSystemPathValidator
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URLDecoder
import java.nio.file.Paths
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/")
class DefaultFileSystemExplorerController(
        private val explorerService: FileSystemExplorerService,
        private val pathValidator: FileSystemPathValidator
) : FileSystemExplorerController {

    @GetMapping("root")
    override fun getRoot(): FileSystemRootEntry = explorerService.getRoot()

    @GetMapping("list/**")
    override fun listPath(request: HttpServletRequest): List<FileSystemEntry> {
        val path =
                URLDecoder.decode(request.requestURI, Charsets.UTF_8.name())
                        .substringAfter("/list/", missingDelimiterValue = "")

        pathValidator.validate(path)

        return explorerService.listPath(Paths.get(path))
    }

}
