package com.linefight.jettree.server.repository.impl.root

import com.linefight.jettree.server.extension.PATH_EMPTY
import com.linefight.jettree.server.extension.getOrEmpty
import com.linefight.jettree.server.repository.FileDescriptor
import com.linefight.jettree.server.repository.exception.PathNotExistsException
import com.linefight.jettree.server.repository.exception.PathNotListableException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Path
import java.nio.file.Paths
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

/**
 * Represents zip-archive file root of file hierarchy
 */
class ZipRoot(parentRoot: Root, relativePathFromParentRoot: Path) : FileRoot(parentRoot, relativePathFromParentRoot) {
    private val log: Logger = LoggerFactory.getLogger(ZipRoot::class.java)

    private val fileDescriptors: Collection<FileDescriptor> by lazy { collectFileDescriptors() }

    override fun listPath(relativePath: Path): List<FileDescriptor> {
        log.debug("Listing path: {}. Root path: {}. Parent root: {}.", relativePath, path, parentRoot)

        validatePath(relativePath)

        val result = fileDescriptors.filter { path.relativize(it.basePath) == relativePath }

        log.debug("File descriptors: {}", result)

        return result.toList()
    }

    override fun getFileDescriptor(relativePath: Path): FileDescriptor =
            findFileDescriptor(relativePath) ?: throw PathNotExistsException(path.resolve(relativePath))

    override fun inputStreamFor(relativePath: Path): ZipInputStream {
        log.debug("Getting input stream for path: {}. Root path: {}", relativePath, path)

        val inputStream = inputStreamForThis()

        val zipInputStream = ZipInputStream(inputStream)

        var zipEntry = zipInputStream.nextEntry
        while (zipEntry != null) {
            if (Paths.get(zipEntry.name) == relativePath) {
                return zipInputStream
            }
            zipInputStream.closeEntry()
            zipEntry = zipInputStream.nextEntry
        }

        zipInputStream.close()

        throw PathNotExistsException(path.resolve(relativePath))
    }

    private fun collectFileDescriptors(): Collection<FileDescriptor> {
        val zipEntries = collectZipEntries()

        val directoryDescriptors = zipEntries.flatMap { it.extractDirectories() }.toSet()
        val fileDescriptors = zipEntries.map { it.toFileDescriptor() }.toSet()

        return directoryDescriptors + fileDescriptors
    }

    private fun findFileDescriptor(relativePath: Path): FileDescriptor? {
        val fullPath = path.resolve(relativePath)

        return fileDescriptors.find { it.basePath.resolve(Paths.get(it.name)) == fullPath }
    }

    private fun collectZipEntries(): Collection<ZipEntry> {
        log.debug("Collecting zip entries of root: {}", this)

        val inputStream = inputStreamForThis()

        val zipEntries = mutableSetOf<ZipEntry>()
        ZipInputStream(inputStream).use {
            var zipEntry = it.nextEntry

            while (zipEntry != null) {
                log.debug("ZipEntry: {}", zipEntry)
                zipEntries += zipEntry
                it.closeEntry()
                zipEntry = it.nextEntry
            }
        }

        return zipEntries
    }

    private fun ZipEntry.extractDirectories(): Collection<FileDescriptor> {
        val zipEntryPath = Paths.get(name)
        val directories = mutableSetOf<FileDescriptor>()

        var zipEntryPathSegmentAccumulator = path
        for (zipEntryPathSegment in zipEntryPath.take(zipEntryPath.nameCount - 1)) {
            directories.add(
                    FileDescriptor(
                            basePath = zipEntryPathSegmentAccumulator,
                            name = zipEntryPathSegment.toString(),
                            isDirectory = true,
                            isReadable = true
                    )
            )
            zipEntryPathSegmentAccumulator = zipEntryPathSegmentAccumulator.resolve(zipEntryPathSegment)
        }

        return directories
    }

    private fun ZipEntry.toFileDescriptor(): FileDescriptor {
        val zipEntryPath = Paths.get(name)

        return FileDescriptor(
                basePath = path.resolve(zipEntryPath.parent.getOrEmpty()),
                name = zipEntryPath.fileName.toString(),
                isDirectory = isDirectory,
                isReadable = true
        )
    }

    private fun validatePath(relativePath: Path) {
        val fileDescriptor = findFileDescriptor(relativePath)

        fun validatePathExists() {
            if (relativePath != PATH_EMPTY && fileDescriptor == null) {
                throw PathNotExistsException(path.resolve(relativePath))
            }
        }

        fun validatePathListable() {
            if (fileDescriptor != null && !fileDescriptor.isDirectory) {
                throw PathNotListableException(path.resolve(relativePath))
            }
        }

        validatePathExists()
        validatePathListable()
    }

    override fun toString(): String {
        return "ZipRoot() ${super.toString()}"
    }
}