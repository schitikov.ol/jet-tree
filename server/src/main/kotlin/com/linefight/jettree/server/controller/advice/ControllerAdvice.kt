package com.linefight.jettree.server.controller.advice

import com.linefight.jettree.server.exception.ServiceException
import com.linefight.jettree.server.model.ErrorCode
import com.linefight.jettree.server.model.ErrorResponse
import com.linefight.jettree.server.validation.exception.*
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ControllerAdvice {

    @ExceptionHandler(ValidationException::class)
    @Order(0)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun validationExceptionHandler(validationException: ValidationException): ErrorResponse =
            when (validationException) {
                is PathInvalidValidationException -> ErrorResponse(ErrorCode.PATH_INVALID, validationException.message)
                is PathNotExistsValidationException -> ErrorResponse(
                        ErrorCode.PATH_NOT_EXISTS,
                        validationException.message
                )
                is PathNotExpandableValidationException -> ErrorResponse(
                        ErrorCode.PATH_NOT_EXPANDABLE,
                        validationException.message
                )
                is PathNotInRootValidationException -> ErrorResponse(
                        ErrorCode.PATH_NOT_IN_ROOT,
                        validationException.message
                )
            }

    @ExceptionHandler(ServiceException::class)
    @Order(1)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun serviceExceptionHandler(serviceException: ServiceException): ErrorResponse =
            ErrorResponse(ErrorCode.INTERNAL_SERVER_ERROR, serviceException.message)

    @ExceptionHandler(Exception::class)
    @Order(2)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun anyExceptionHandler() = ErrorResponse(ErrorCode.INTERNAL_SERVER_ERROR)
}