package com.linefight.jettree.server.configuration

import com.linefight.jettree.server.model.FileSystemEntryType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("filesystem.entry.type")
class FileSystemEntryTypeProperties(
        archiveExtensions: Set<String>,
        imageExtensions: Set<String>,
        videoExtensions: Set<String>,
        audioExtensions: Set<String>,
        textExtensions: Set<String>,
        execExtensions: Set<String>
) {
    private val log: Logger = LoggerFactory.getLogger(FileSystemEntryTypeProperties::class.java)
    private val extensionToFileSystemEntryTypeMap: Map<String, FileSystemEntryType>

    init {
        val extensionToFileSystemEntryTypeMap = mutableMapOf<String, FileSystemEntryType>()

        registerExtensions(extensionToFileSystemEntryTypeMap, FileSystemEntryType.ARCHIVE, archiveExtensions)
        registerExtensions(extensionToFileSystemEntryTypeMap, FileSystemEntryType.IMAGE, imageExtensions)
        registerExtensions(extensionToFileSystemEntryTypeMap, FileSystemEntryType.VIDEO, videoExtensions)
        registerExtensions(extensionToFileSystemEntryTypeMap, FileSystemEntryType.AUDIO, audioExtensions)
        registerExtensions(extensionToFileSystemEntryTypeMap, FileSystemEntryType.TEXT, textExtensions)
        registerExtensions(extensionToFileSystemEntryTypeMap, FileSystemEntryType.EXEC, execExtensions)

        this.extensionToFileSystemEntryTypeMap = extensionToFileSystemEntryTypeMap
    }

    fun getFileSystemEntryType(extension: String): FileSystemEntryType? = extensionToFileSystemEntryTypeMap[extension]

    private fun registerExtensions(
            extensionToFileSystemEntryTypeMap: MutableMap<String, FileSystemEntryType>,
            fileSystemEntryType: FileSystemEntryType,
            extensions: Set<String>
    ) {
        log.info("Registering $fileSystemEntryType extensions: {}", extensions)
        extensionToFileSystemEntryTypeMap.putAll(extensions.associateWith { fileSystemEntryType })
    }
}