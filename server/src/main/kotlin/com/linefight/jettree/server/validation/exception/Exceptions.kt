package com.linefight.jettree.server.validation.exception

import com.linefight.jettree.server.exception.ServiceException
import com.linefight.jettree.server.extension.invariantSeparatorsPath
import java.nio.file.Path

sealed class ValidationException(message: String? = null, cause: Throwable? = null) :
        ServiceException(message, cause)

class PathInvalidValidationException(
        path: String,
        cause: Throwable? = null
) : ValidationException("Path is invalid. Path: $path", cause)

class PathNotExistsValidationException(path: Path) : ValidationException("Path not exists. Path: ${path.invariantSeparatorsPath}")

class PathNotExpandableValidationException(path: Path) : ValidationException("Path not expandable. Path: ${path.invariantSeparatorsPath}")

class PathNotInRootValidationException(path: Path) : ValidationException("Path must be contained in the root. Path: ${path.invariantSeparatorsPath}")