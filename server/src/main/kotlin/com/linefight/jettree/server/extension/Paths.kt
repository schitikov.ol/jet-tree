package com.linefight.jettree.server.extension

import java.nio.file.Path
import java.nio.file.Paths

val PATH_EMPTY: Path = Paths.get("")

val Path.invariantSeparatorsPath: String get() = toFile().invariantSeparatorsPath
val Path.extension: String get() = toFile().extension
fun Path?.getOrEmpty(): Path = this ?: PATH_EMPTY