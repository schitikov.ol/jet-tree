package com.linefight.jettree.server.service

import com.linefight.jettree.server.model.FileSystemEntry
import com.linefight.jettree.server.model.FileSystemRootEntry
import java.nio.file.Path

/**
 * Interface for file hierarchy explorer of some filesystem
 */
interface FileSystemExplorerService {
    /**
     * @param path relative path to [getRoot] entry
     * @return [List] of [FileSystemEntry] contained in [path]
     */
    fun listPath(path: Path): List<FileSystemEntry>

    /**
     * @return [FileSystemRootEntry] representing a root of exploring filesystem
     */
    fun getRoot(): FileSystemRootEntry
}