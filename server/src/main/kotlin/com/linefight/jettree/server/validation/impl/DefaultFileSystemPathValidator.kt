package com.linefight.jettree.server.validation.impl

import com.linefight.jettree.server.validation.FileSystemPathValidator
import com.linefight.jettree.server.validation.exception.PathInvalidValidationException
import com.linefight.jettree.server.validation.exception.PathNotInRootValidationException
import org.springframework.stereotype.Component
import java.nio.file.Path
import java.nio.file.Paths

@Component
/**
 * Represent path validator relative to [rootPath]
 * @property rootPath Absolute [Path] to the file hierarchy root
 */
class DefaultFileSystemPathValidator(private val rootPath: Path) : FileSystemPathValidator {

    override fun validate(path: String) {
        validate(
                runCatching { Paths.get(path) }.onFailure {
                    throw PathInvalidValidationException(path, it)
                }.getOrThrow()
        )
    }

    override fun validate(path: Path) {
        val fullPath = rootPath.resolve(path).normalize()

        if (!fullPath.startsWith(rootPath)) throw PathNotInRootValidationException(fullPath)
    }
}